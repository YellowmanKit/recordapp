import React from 'react'
import Main from 'recordapp/src/component/main/Main'

import Func from './utility/Func'
import Input from './component/native/prefab/Input'
import Button from './component/native/prefab/Button'
import Text from './component/native/prefab/Text'
import Element from './component/native/prefab/Element'

class Port extends React.Component {

  render = () => this.port(this.props)

  port({ store, action }){
    const props = { app: { store, action, func: new Func({ app: { store }}) } }
    return(
      <Main app={{
        ...this.state, ...props.app,
        func: new Func(props),
        text: new Text(props),
        input: new Input(props),
        button: new Button(props),
        element: new Element(props)
      }}/>
    )
  }

}

import actions from './redux/actions'
import { connect } from 'react-redux'
export default connect(store => { return { store } }, dispatch => { return actions(dispatch) })(Port)
