import React from 'react'
import { View as Div } from 'react-native'

export default class Base extends React.Component {

  horGap = widthScale => this.gap(this.ui, { widthScale })
  verGap = heightScale => this.gap(this.ui, { heightScale })
  gap = ({ window }, { widthScale, heightScale }) =>
    <Div style={{
      flexShrink: 0,
      width: window.width * (widthScale? widthScale: 0),
      height: window.height * (heightScale?  heightScale: 0)
    }}/>

  size = (widthScale, heightScale, useWidth) => {
    return {
      width: (useWidth? this.ui.window.width: this.ui.window.height) * widthScale,
      height: this.ui.window.height * heightScale
    }
  }

  sizePercent(widthPercent, heightPercent){
    return { width: '' + (widthPercent * 100) + '%',
      height: '' + (heightPercent * 100) + '%'}
  }

}
