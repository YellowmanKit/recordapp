import React from 'react'
import { View as Div } from 'react-native'
import Component from 'recordapp/src/component/Component'

export default class Loading extends Component {

  content = ({ store: { main: { loading }, ui: { window, style: { flex } } }, action, text, button, element }) =>
    loading?<Div style={{ ...window, ...flex.ColCC, position: 'absolute',
    backgroundColor: 'white', opacity: 0.85 }}>
      {text.title('Loading...')}
    </Div>:null

}
