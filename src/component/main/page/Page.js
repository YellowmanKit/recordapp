import React from 'react'
import { SafeAreaView as Div, BackHandler } from 'react-native'
import Component from 'recordapp/src/component/Component'
import Index from './view/index/Index'
import Survey from './view/survey/Survey'
import Report from './view/report/Report'
import ViewSurvey from './view/viewSurvey/ViewSurvey'
import ViewReport from './view/viewReport/ViewReport'

export default class Page extends Component {

  content = ({ store: { ui: { window, style: { flex } } } }) =>
    <Div style={{ ...window, ...flex.ColCC, backgroundColor: 'white' }}>
      {this.page(this.app)}
    </Div>

  page = ({ store: { page: { view } } }) => {
    switch (view) {
      case 'index':
        return <Index app={this.app}/>
      case 'survey':
        return <Survey app={this.app}/>
      case 'report':
        return <Report app={this.app}/>
      case 'viewSurvey':
        return <ViewSurvey app={this.app}/>
      case 'viewReport':
        return <ViewReport app={this.app}/>
      default:
        return null
    }
  }

}
