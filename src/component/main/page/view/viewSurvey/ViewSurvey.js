import React from 'react'
import { View as Div, ScrollView } from 'react-native'
import Component from 'recordapp/src/component/Component'
import Top from 'recordapp/src/component/module/Top'

export default class ViewSurvey extends Component {

  content = ({ store: { ui: { window, style: { flex } }, survey: { surveys, selectedSurvey } },
    action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCS }}>
      {this.verGap(0.02)}
      <Top app={this.app} title={'View Survey'}/>
      <ScrollView style={{ ...this.size(0.9,0.9,true) }}>
        <Div style={{ ...flex.ColSS }}>
          {Object.keys(surveys[selectedSurvey]).map(key => text.record(
            key + ' : ' + surveys[selectedSurvey][key], key))}
        </Div>
      </ScrollView>
      {this.verGap(0.03)}
      {button.standard('Delete', ()=>action.survey.deleteSurvey(surveys[selectedSurvey].id))}
      {this.verGap(0.03)}
    </Div>

}
