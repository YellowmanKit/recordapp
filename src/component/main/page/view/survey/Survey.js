import React from 'react'
import { View as Div, ScrollView } from 'react-native'
import Component from 'recordapp/src/component/Component'
import Top from 'recordapp/src/component/module/Top'

export default class Survey extends Component {

  init({ action: { survey }}){
    survey.fetchSurveys()
  }

  content = ({ store: { ui: { window, style: { flex } }, survey }, action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCS }}>
      {this.verGap(0.02)}
      <Top app={this.app} title={'Survey'}/>
      <ScrollView style={{ ...this.size(1,0.9,true) }}>
        <Div style={{ ...flex.ColCS }}>
          {survey.surveys.map((record, index) => button.record(
            record.id,
            ()=>{
              action.survey.set('selectedSurvey', index)
              action.page.push('viewSurvey')
            }, record.id))}
        </Div>
      </ScrollView>
    </Div>

}
