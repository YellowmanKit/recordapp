import React from 'react'
import { View as Div, ScrollView } from 'react-native'
import Component from 'recordapp/src/component/Component'
import Top from 'recordapp/src/component/module/Top'

export default class ViewReport extends Component {

  content = ({ store: { ui: { window, style: { flex } }, report: { reports, selectedReport } },
    action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCS }}>
      {this.verGap(0.02)}
      <Top app={this.app} title={'View Report'}/>
      <ScrollView style={{ ...this.size(0.9,0.9,true) }}>
        <Div style={{ ...flex.ColSS }}>
          {Object.keys(reports[selectedReport]).map(key => text.record(
            key + ' : ' + reports[selectedReport][key], key))}
        </Div>
      </ScrollView>
      {this.verGap(0.03)}
      {button.standard('Delete', ()=>action.report.deleteReport(reports[selectedReport].id))}
      {this.verGap(0.03)}
    </Div>

}
