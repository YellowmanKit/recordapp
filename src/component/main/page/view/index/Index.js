import React from 'react'
import { View as Div, BackHandler } from 'react-native'
import Component from 'recordapp/src/component/Component'

export default class Index extends Component {

  content = ({ store: { ui: { window, style: { flex } } }, action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCS }}>
      {this.verGap(0.1)}
      {element.logo(require('recordapp/res/image/hku.png'))}
      {this.verGap(0.02)}
      {text.logo('Record Viewer')}
      {this.verGap(0.2)}
      {button.entry('Survey Record', 'green', ()=>{
        action.page.push('survey')
      })}
      {this.verGap(0.02)}
      {button.entry('Report Record', 'blue', ()=>{
        action.page.push('report')
      })}
      {this.verGap(0.02)}
      {button.entry('Quit System', 'red', ()=>BackHandler.exitApp())}
    </Div>

}
