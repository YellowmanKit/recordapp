import React from 'react'
import { View as Div, ScrollView } from 'react-native'
import Component from 'recordapp/src/component/Component'
import Top from 'recordapp/src/component/module/Top'

export default class Report extends Component {

  init({ action: { report }}){
    report.fetchReports()
  }

  content = ({ store: { ui: { window, style: { flex } }, report }, action, text, button, element }) =>
    <Div style={{ ...window, ...flex.ColCS }}>
      {this.verGap(0.02)}
      <Top app={this.app} title={'Report'}/>
      <ScrollView style={{ ...this.size(1,0.9,true) }}>
        <Div style={{ ...flex.ColCS }}>
          {report.reports.map((record, index) => button.record(
            record.id,
            ()=>{
              action.survey.set('selectedReport', index)
              action.page.push('viewReport')
            }, record.id))}
        </Div>
      </ScrollView>
    </Div>

}
