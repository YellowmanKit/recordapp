import React from 'react'
import Component from 'recordapp/src/component/Component'
import { View as Div, Dimensions, StatusBar, Platform } from 'react-native'
import Page from './page/Page'
import Loading from './mask/Loading'

export default class Main extends Component {

  init({ action }){
    var size = Dimensions.get('window')
    if(Platform.OS === 'android'){
      size.height += StatusBar.currentHeight
    }
    action.main.set('status', 'ready')
    action.ui.set('window', size )
  }

  content = ({ store: { ui: { window, style: { flex } } } }) =>
    <Div style={{ ...window, ...flex.ColCC }}>
      {this.main(this.app)}
      <Loading app={this.app}/>
    </Div>

  main = ({ store: { main: { status } } }) => {
    switch (status) {
      case 'ready':
        return <Page app={this.app}/>
      default:
        return null
    }
  }

}
