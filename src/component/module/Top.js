import React from 'react'
import { View as Div } from 'react-native'
import Component from 'recordapp/src/component/Component'

export default class Top extends Component {

  content = ({ store: { report, ui: { window, style: { flex } } },
    action, button, text }, { title }) =>
    <Div style={{ ...this.size(1, 0.1), ...flex.RowCC }}>
      {button.top('<', ()=>action.page.pull())}
      {this.horGap(0.1)}
      {text.top(title)}
      {this.horGap(0.175)}
    </Div>

}
