import React from 'react'
import Base from './Base'

export default class Component extends Base {

  constructor(props){
    super(props)
    this.state = {}
    this.update(props)
    if(this.init){ this.init(props.app, props) }
  }

  update(props){
    this.app = props.app
    this.store = props.app.store
    this.ui = props.app.store.ui
    this.action = props.app.action
    this.flex = this.ui.style.flex
    this.text = this.app.text
    this.input = this.app.input
    this.button = this.app.button
  }

  render(){
    this.update(this.props)
    return this.content(this.props.app, this.props)
  }

}
