import React from 'react'
import Component from 'recordapp/src/component/Component'
import { View as Div, Text, Image, TouchableOpacity } from 'react-native'
import { TextInput } from 'react-native-normalized'

export default class Native extends Component {

  image = (source, style) =>
    <Image style={style} source={source}/>

  text = ({ text, key }, style) =>
    <Text style={style} key={key}>{text}</Text>

  background = url =>
    <Image style={{ position: 'absolute', width: '100%', height: '100%', resizeMode: 'stretch'  }} source={url?url:null} />

  checkbox = ({ value, onPress }, style) =>
    <TouchableOpacity style={{...{ backgroundColor: 'white', borderWidth: 1, borderColor: 'black', borderRadius: 3 }, ...style}} onPress={onPress}>
      {value && this.background(require('recordapp/res/image/tick.png')) }
    </TouchableOpacity>

  checkboxBar = (option, style) =>
    <Div style={style.view}>
      {this.checkbox(option, style.checkbox)}
      {this.horGap(0.02)}
      {this.text(option, style.text)}
    </Div>

  textArea = ({ value, onChangeText, multiline }, style) =>
      <TextInput
        value={value? value: ''}
        onChangeText={onChangeText}
        style={style}
        autoCapitalize={'none'}
        multiline={multiline}
        textAlignVertical={'top'}
      />

  touchableOpacity = ({ text, onPress, key }, style) =>
    <TouchableOpacity style={{ ...this.ui.style.flexColCC, ...style.view }} onPress={onPress} key={key}>
      {text && this.text({ text }, style.text)}
    </TouchableOpacity>

}
