import React from 'react'
import Native from '../Native'

export default class Button extends Native{

  record = (text, onPress, key) =>
    this.touchableOpacity({ text, onPress, key }, {
      view: { ...this.size(0.85,0.05,true), backgroundColor: 'white', borderWidth: 1,
      borderRadius: 4, margin: 3 },
      text: { fontSize: 20, padding: 3, textAlign: 'center' }
    })

  standard = (text, onPress) =>
    this.touchableOpacity({ text, onPress }, {
      view: { backgroundColor: 'white', borderWidth: 1, borderRadius: 4 },
      text: { fontSize: 20, padding: 3 }
    })

  nav = (text, onPress) =>
    this.touchableOpacity({ text, onPress }, {
      view: { backgroundColor: 'white' },
      text: { fontSize: 20, padding: 5 }
    })

  top = (text, onPress) =>
    this.touchableOpacity({ text, onPress }, {
      view: { backgroundColor: 'white', borderWidth: 1, borderRadius: 4 },
      text: { fontSize: 25, padding: 5 }
    })

  entry = (text, color, onPress) =>
    this.touchableOpacity({ text, onPress }, {
      view: { ...this.size(0.35,0.075), backgroundColor: color, borderRadius: 8 },
      text: { fontSize: 15, padding: 15, color: 'white', textAlign: 'center' }
    })


}
