import React from 'react'
import Native from '../Native'

export default class Text extends Native {

  record = (text, key) => this.text({ text, key }, { fontSize: 15 })

  small = (text) => this.text({ text, key }, { fontSize: 15 })

  top = text => this.text({ text }, { fontSize: 28, textAlign: 'center',
    fontWeight: 'bold',  ...this.size(0.6, 0.06, true) })

  title = text => this.text({ text }, { fontSize: 25, fontWeight: 'bold' })

  logo = text => this.text({ text }, { fontSize: 15, fontWeight: 'bold' })

  standard = text => this.text({ text }, { fontSize: 20 })

}
