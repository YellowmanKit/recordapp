import { bindActionCreators } from 'redux'
import * as main from './control/main/action'
import * as ui from './control/ui/action'
import * as page from './control/page/action'

import * as survey from './data/survey/action'
import * as report from './data/report/action'

const add = (action, dispatch) => {
  action.init = (key, value) => { return { type: 'init', payload: { key, value } } }
  action.set = (key, value) => { return { type: 'set', payload: { key, value } } }
  return bindActionCreators(action, dispatch)
}

export default actions = dispatch => { return { action: {
  main: add(main, dispatch),
  ui: add(ui, dispatch),
  page: add(page, dispatch),

  survey: add(survey, dispatch),
  report: add(report, dispatch)
}}}
