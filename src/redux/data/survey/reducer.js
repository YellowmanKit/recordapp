import { reducer } from '../../redux'

export const survey = (
  state = initState,
  action) => reducer(state, action, result)

const initState = {
  surveys: [],
  selectedSurvey: 0
}

const result = (state, { type, payload }) => {
  switch (type) {
    default:
      return state
  }
}
