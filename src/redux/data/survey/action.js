import axios from 'axios'
import { to, action, IOS } from '../../redux'
import { API } from '@env'

var err, res
export const fetchSurveys = coordinate => {
  return async dispatch => {
    [err, res] = await to(axios.post(API + 'record'))
    dispatch(action.set('surveys', res.data.result))
  }
}

export const deleteSurvey = id => {
  return async dispatch => {
    dispatch(action.set('loading', true));
    [err, res] = await to(axios.post(API + 'delete/' + id))
    dispatch(action.set('loading', false))
    dispatch(action.pull())
  }
}
