import axios from 'axios'
import { to, action, IOS } from '../../redux'
import { API2 } from '@env'

var err, res
export const fetchReports = coordinate => {
  return async dispatch => {
    [err, res] = await to(axios.post(API2 + 'report'))
    dispatch(action.set('reports', res.data.result))
  }
}

export const deleteReport = id => {
  return async dispatch => {
    dispatch(action.set('loading', true));
    [err, res] = await to(axios.post(API2 + 'delete/' + id))
    dispatch(action.set('loading', false))
    dispatch(action.pull())
  }
}
