import { reducer } from '../../redux'

export const report = (
  state = initState,
  action) => reducer(state, action, result)

const initState = {
  reports: [],
  selectedReport: 0
}

const result = (state, { type, payload }) => {
  switch (type) {
    default:
      return state
  }
}
