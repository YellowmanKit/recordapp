import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { main } from './control/main/reducer'
import { ui } from './control/ui/reducer'
import { page } from './control/page/reducer'

import { survey } from './data/survey/reducer'
import { report } from './data/report/reducer'

const reducer = combineReducers({
  main, ui, page,
  survey, report
})

export default createStore(reducer, {}, applyMiddleware(thunk))
