import { Platform } from 'react-native'
export const IOS = Platform.OS === 'ios'
import axios from 'axios'
export const to = promise => promise.then(data => [null, data] ).catch(err => [err])
export const action = {
  set: (key, value) => { return { type: 'set', payload: { key, value } } },
  define: state => { return { type: 'define', payload: state } },
  update: (key, value, append, remove) => { return { type: 'update', payload: { key, value, append, remove } } },
  pull: () => { return { type: 'pull' }}
}
export const reducer = (state, { type, payload }, result) => {
  switch (type) {
    case 'init':
      state[payload.key] = payload.value
      return { ...state }
    case 'set':
      if(payload.value === undefined){ return state }
      if(payload.key in state){ state[payload.key] = payload.value }
      return { ...state }
    case 'update':
      if(payload.key in state){ state[payload.key] = update(state[payload.key], payload.value, payload.append, payload.remove) }
      return { ...state }
    default:
      return result(state, { type, payload })
  }
}
const update = (original, newItems, append, remove) => {
  if(!newItems || newItems.length === 0){ return original }
  var result = original.slice(0)
  if(result.length === 0){ result = [newItems[0]] }
  for(var i=0;i<newItems.length;i++){
    if(!newItems[i]){ continue }
    for(var j=0;j<result.length;j++){
      if(result[j]._id && (result[j]._id === newItems[i]._id)){
        if(remove){
          result.splice(j, 1)
        }else{
          result[j] = { ...result[j], ...newItems[i] }
        }
        break
      }
      if(!result[j]._id && (result[j] === newItems[i])){
        if(remove){
          result.splice(j, 1)
        }else{
          result[j] = newItems[i]
        }
        break
      }
      if(j === result.length - 1){
        if(append){ result.splice(result.length, 0, newItems[i]) }
        else{ result.splice(0, 0, newItems[i]); break }
      }
    }
  }
  return result
}
