import React from 'react';
import { Provider } from 'react-redux';
import store from './src/redux/store';
import Port from './src/Port';

export default class App extends React.Component {
  render = () => <Provider store={store}><Port/></Provider>
}
